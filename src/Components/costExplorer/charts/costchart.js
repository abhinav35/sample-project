import React from 'react'
import { Bar } from 'ant-design-pro/lib/Charts';
import {salesData} from './charts';
import arrowdown from '../../../assests/arrowdown.svg'


const costChart=()=>{
    return(
        <div>
                    <div>
                        <h5>Daily Events in mn</h5>
                    </div>
                    <div>
                        <div className="dropdown mr-5" style={{display:'inline-block'}}>
                            <button type="button" className="btn btn-secondary" style={{padding:'2px',textAlign:'left'}} data-toggle="dropdown">
                                <span className='ml-1' style={{fontSize:'13px'}}>Last 30 Days</span>
                                <span style={{marginLeft:'6rem'}}><img src={arrowdown} alt='arrow down'/></span>
                            </button>
                            <div className="dropdown-menu">
                                <p className="dropdown-item" >Link 1</p>
                                <p className="dropdown-item" >Link 2</p>
                                <p className="dropdown-item" >Link 3</p>
                            </div>
                        </div>
                        <div className="dropdown ml-5" style={{display:'inline-block'}}>
                            <button type="button" className="btn btn-secondary" style={{padding:'2px',textAlign:'left'}} data-toggle="dropdown">
                                <span className='ml-1' style={{fontSize:'13px'}}>Daily</span>
                                <span  style={{marginLeft:'8.5rem'}}><img src={arrowdown} alt='arrow down'/></span>
                            </button>
                            <div className="dropdown-menu">
                                <p className="dropdown-item" >Link 1</p>
                                <p className="dropdown-item" >Link 2</p>
                                <p className="dropdown-item" >Link 3</p>
                            </div>
                        </div>
                    </div>
                    <div className='my-4 '>
                        <span>Filters</span>
                        <div className="dropdown ml-5" style={{display:'inline-block'}}>
                            <button type="button" className="btn btn-outline-secondary" style={{padding:'2px',textAlign:'left',borderRadius:'100px'}} data-toggle="dropdown">
                                <span className='ml-1' style={{fontSize:'15px'}}>Services</span>
                                <span  style={{marginLeft:'1rem'}}><img src={arrowdown} alt='arrow down'/></span>
                            </button>
                            <div className="dropdown-menu">
                                <p className="dropdown-item" >Link 1</p>
                                <p className="dropdown-item" >Link 2</p>
                                <p className="dropdown-item" >Link 3</p>
                            </div>
                        </div>
                        <div className="dropdown ml-5" style={{display:'inline-block'}}>
                            <button type="button" className="btn btn-outline-secondary" style={{padding:'2px',textAlign:'left',borderRadius:'100px'}} data-toggle="dropdown">
                                <span className='ml-1' style={{fontSize:'15px'}}>Endpoint</span>
                                <span  style={{marginLeft:'1rem'}}><img src={arrowdown} alt='arrow down'/></span>
                            </button>
                            <div className="dropdown-menu">
                                <p className="dropdown-item" >Link 1</p>
                                <p className="dropdown-item" >Link 2</p>
                                <p className="dropdown-item" >Link 3</p>
                            </div>
                        </div>
                    </div>
                    <Bar height={400} width={10}  data={salesData} />
                </div>
    )
}

export default costChart
