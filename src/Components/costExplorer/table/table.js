import React from 'react';
import data from './tableData';
import {NavLink} from 'react-router-dom'

const table=()=>{
    return(
        <div className='Table'>
             <table className="table">
  <thead>
    <tr>
        {data.map(key=><th key={key.key}>{key.title}</th>)}
    </tr>
  </thead>
  <tbody>
      {data.map(details=>
      <tr key={details.key}>
        <NavLink to='/'><strong>{details.services}</strong></NavLink>
        <td>{details.totalAnalysedData}</td>
        <td>{details.filteringRate}</td>
        <td>{details.totalRetainedSpans}</td>
      </tr>
      )}
  </tbody>
</table>
        </div>
    )
}

export default table



