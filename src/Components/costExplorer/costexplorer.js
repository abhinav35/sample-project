import React,{Component} from 'react';
import Table from './table/table'
import DailyEvent from './charts/dailyeventchart'
import CostChart from './charts/costchart'


class CostExplorer extends Component{
    render() {
        return (
            <div className='container'>
                <div style={{textAlign:'center',marginTop:'75px'}} className='mb-3'>

                    <button className='btn btn-primary' style={{marginRight:'100px',marginBottm:'50px',padding:'15px 12px 9px 12px'}}>
                        <h2 style={{color:'white'}}>212k </h2>
                        <h6 style={{color:'white',margin:'5px'}}>retained spans/month</h6>
                        <span style={{fontSize:'10px',margin:'0px'}}>(predicted monthly estimate)</span>
                    </button>

                    <button className='btn btn-primary' style={{padding:'15px 25px 9px 25px'}}>
                    <h2 style={{color:'white'}}>USD 436</h2>
                        <span style={{fontSize:'10px',margin:'0px'}}>(predicted monthly cost<br/>
                         based on current estimate)</span>
                        <span></span>
                    </button>
                </div>
                <div>
                    <h5 style={{fontWeight:'700'}}>Cost Explorer</h5>
                    <Table/>
                    <div className='m-3 p-4 ' style={{border:'1px solid black'}}><CostChart/></div>
                    <div className='m-3 p-4' style={{border:'1px solid black'}}><DailyEvent/></div>

                </div>
                
            </div>
        )
    }
}

export default CostExplorer