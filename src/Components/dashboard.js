import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import {DesktopOutlined,PieChartOutlined} from '@ant-design/icons';
import {NavLink} from 'react-router-dom'
import ComponentLoader from './ComponentLoader'

const { Header, Content, Footer, Sider } = Layout;

class SiderDemo extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1" icon={<PieChartOutlined />}>
                <NavLink to='application'>Application</NavLink>
            </Menu.Item>
            <Menu.Item key="2" icon={<DesktopOutlined />}>
            <NavLink to='/infrastructure'>Infrastructure</NavLink>
            </Menu.Item>
            <Menu.Item key="3" icon={<DesktopOutlined />}>
            <NavLink to='/'>Traces</NavLink>
            </Menu.Item>            
            <Menu.Item key="4" icon={<DesktopOutlined />}>
            <NavLink to='/'>Service Map</NavLink>
            </Menu.Item>            
            <Menu.Item key="5" icon={<DesktopOutlined />}>
            <NavLink to='/'>Alerts</NavLink>
            </Menu.Item>
            <Menu.Item key="6" icon={<DesktopOutlined />}>
            <NavLink to='/costexplorer'>Cost Explorer</NavLink>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
            </Breadcrumb>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
              <ComponentLoader/>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>SigNoz</Footer>
        </Layout>
      </Layout>
    );
  }
}

export default SiderDemo