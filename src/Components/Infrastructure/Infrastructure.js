import React, { Component } from 'react';
import Nodes from './Nodes/Nodes';
import Namespaces from './Namespaces/Namespaces';
import Cluster from './Clusters/Cluster'



class Infrastructure extends Component{
    state={
        loadlink:''
    }

    LoadNodeHandler=async()=>{
        await this.setState({
            loadlink:'Node'
        })
    }

    LoadClusterHandler=async()=>{
        await this.setState({
            loadlink:'Cluster'
        })
    }

    LoadNamespacesHandler=async()=>{
        await this.setState({
            loadlink:'Namespaces'
        })
    }

    render() {
        let loadComponent
            switch (this.state.loadlink) {
                case 'Cluster':
                    loadComponent=<Cluster/>
                    break;
                case 'Namespaces':
                    loadComponent=<Namespaces/>
                    break;
                default:
                    loadComponent=<Nodes/>
                    break;
            }
        return (
            <div>
                <div class="container">
                    <nav className="navbar navbar-expand-lg ">
                        <h5 className="navbar-brand" onClick={this.LoadNodeHandler}><strong>Nodes</strong></h5>
                        <h5 className="navbar-brand" onClick={this.LoadClusterHandler}><strong>Clusters</strong></h5>
                        <h5 className="navbar-brand" onClick={this.LoadNamespacesHandler}><strong>Namespaces</strong></h5>
                    </nav>
                    <div>
                        {loadComponent}
                    </div>
                </div>
            </div>
        )
    }
}

export default Infrastructure