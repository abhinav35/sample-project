import React from 'react'
import Nodetable from './tabledata';
import {NavLink} from 'react-router-dom'

const table=()=>{
    return(
        <table class="table">
                <thead>
                    <tr>
                        {Nodetable.map(heading=><th scope="col">{heading.heading}</th>)}    
                    </tr>
                </thead>
                <tbody>
                    {
                        Nodetable.map(data=>
                            <tr>
                                <td><NavLink to='/'><strong>{data.Hostname}</strong></NavLink></td>
                                <td>{data.CPUusage}</td>
                                <td>{data.Memoryusage}</td>
                                <td>{data.IOLoad}</td>
                                <td>{data.Application}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
    )
}

export default table