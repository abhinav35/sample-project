import  React,{Component} from 'react'
import Table from './Table/table'
import arrowdown from '../../assests/arrowdown.svg'

class Application extends Component{
    render() {
        return (
            <div>
                <div style={{textAlign:'right'}}>
                    <div className="dropdown ml-5" style={{display:'inline-block'}}>
                        <button type="button" className="btn btn-outline-secondary mb-5" style={{textAlign:'left'}} data-toggle="dropdown">
                            <span className='ml-1' style={{fontSize:'15px'}}>Services</span>
                            <span  style={{marginLeft:'8rem'}}><img src={arrowdown} alt='arrow down'/></span>
                        </button>
                        <div className="dropdown-menu">
                            <p className="dropdown-item" >Link 1</p>
                            <p className="dropdown-item" >Link 2</p>
                            <p className="dropdown-item" >Link 3</p>
                        </div>
                    </div>
                </div>
                <Table/>
            </div>
        )
    }
}

export default Application