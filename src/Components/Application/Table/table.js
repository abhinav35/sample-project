import React from 'react';
import Applicationdata from './tableData';
import {NavLink} from 'react-router-dom'

const table=()=>{
    return(
        <div>
            <table class="table">
                <thead>
                    <tr>
                        {Applicationdata.map(heading=><th scope="col">{heading.heading}</th>)}    
                    </tr>
                </thead>
                <tbody>
                    {
                        Applicationdata.map(data=>
                            <tr>
                                <td><NavLink to='/'><strong>{data.application}</strong></NavLink></td>
                                <td>{data.p95latency}</td>
                                <td>{data.errorrate}</td>
                                <td>{data.requestpersec}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default table
