import React, { Component } from 'react'
import Graph from './Graph/graphs';
import CostExplorer from './costExplorer/costexplorer'
import Application from './Application/Application'
import Infrastructure from './Infrastructure/Infrastructure'
import Traces from './Traces/Traces'


class ComponentLoader extends Component{

    render() {
        let componentLoaded

        let loadedRoute=window.location.pathname

        switch (loadedRoute) {
            case '/costexplorer':
                componentLoaded=<CostExplorer/>
            break;
            case '/infrastructure':
                componentLoaded=<Infrastructure/>
            break
            case '/application':
                componentLoaded=<Application/>
            break
            default:
                componentLoaded=<Graph/>
            break;
        }

        return (
            <div>
                <Traces/>
                {/* {componentLoaded} */}
            </div>
        )
    }
}

export default ComponentLoader