import React, { Component, createRef } from 'react';
import { Line } from '@ant-design/charts';

class pieChart extends Component {
    constructor(props){
        super(props)
        this.ref = createRef();
    }
  // DownloadImage
//   downloadImage = () => {
//     this.ref.current?.downloadImage();
//   };

  // Get data base64
  toDataURL = () => {
    console.log(this.ref.current?.toDataURL());
  };

  render() {
    const data = [
      { year: '1991', value: 3 },
      { year: '1992', value: 4 },
      { year: '1993', value: 3.5 },
      { year: '1994', value: 5 },
      { year: '1995', value: 4.9 },
      { year: '1996', value: 6 },
      { year: '1997', value: 7 },
      { year: '1998', value: 9 },
      { year: '1999', value: 13 },
    ];

    const config = {
      data,
      title: {
        visible: true,
        text: 'Line chart with data points',
      },
      xField: 'year',
      yField: 'value',
    };

    return (
      <div>
        <button type="button" className='btn btn-primary' onClick={this.downloadImage} style={{ marginRight: 24 }}>
          Download Image
        </button>
        <button type="button" className='btn btn-primary' onClick={this.toDataURL}>
        Get picture information
        </button>
        <Line {...config} chartRef={this.ref} />
      </div>
    );
  }
}
export default pieChart;