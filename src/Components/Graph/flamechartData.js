const data = {
    name: 'root',
    value: 15,
    children: [
      {
        name: 'Java',
        value: 11.4,
        tooltip: 'Custom tooltip shown on hover',
        children:[
            {
                name: 'start_thread',
                value:8,
                tooltip:"start_thread",
                children:[
                    {
                        name: 'start_thread',
                        value:8,
                        tooltip:"start_thread",
                        children:[
                            {
                                name: 'start_thread',
                                value:5,
                                tooltip:"start_thread",
                            },
                            {
                                name: 'start_thread',
                                value:2,
                                tooltip:"start_thread",
                                backgroundColor: '#4BB0A5',                    
                            },
                            {
                                name: 'start_thread',
                                value:1,
                                tooltip:"start_thread",
                                backgroundColor: '#4BB0A5',                    
                            }
                        ]
                    }
                ]
            },
            {
                name:"_lib_send",
                value:2,
                tooltip:"_lib_send",
                backgroundColor: '#4BB0A5',
                children:[
                    {
                        name:"_lib_send",
                        value:1.8,
                        tooltip:"_lib_send",
                        backgroundColor: '#4BB0A5',
                        children:[
                            {
                                name:"_lib_send",
                                value:0.8,
                                tooltip:"_lib_send",
                                backgroundColor: '#4BB0A5',
                            },
                            {
                                name:"_lib_send",
                                value:0.10,
                                tooltip:"_lib_send",
                                backgroundColor: '#4BB0A5',
                            }
                        ]                    
                    },
                    {
                        name:"_lib_send",
                        value:0.2,
                        tooltip:"_lib_send",
                        backgroundColor: '#4BB0A5',
                        children:[
                            {
                                name:"_lib_send",
                                value:0.2,
                                tooltip:"_lib_send",
                                backgroundColor: '#4BB0A5',
                            }
                        ]
                    }
                ]
            },
            {
                name: 'unknown',
                value:1,
                tooltip:"unknown",
                backgroundColor: '#4BB0A5',
                children:[
                    {
                        name: 'unknown',
                        value:0.33,
                        tooltip:"unknown",
                        backgroundColor: '#4BB0A5',
                    },
                    {
                        name: 'unknown',
                        value:0.33,
                        tooltip:"unknown",
                        backgroundColor: '#4BB0A5',
                    },
                    {
                        name: 'unknown',
                        value:0.33,
                        tooltip:"unknown",
                        backgroundColor: '#4BB0A5',
                    }
                ]
            },
            {
                name: 'unknown',
                value:0.2,
                tooltip:"unknown",
                backgroundColor: '#4BB0A5',
                children:[
                    {
                        name: 'unknown',
                        value:0.2,
                        tooltip:"unknown",
                        backgroundColor: '#4BB0A5',
                    }
                ]
            },
            {
                name: 'unknown',
                value:0.1,
                tooltip:"unknown",
                backgroundColor: '#4BB0A5',
                children:[
                    {
                        name: 'unknown',
                        value:0.1,
                        tooltip:"unknown",
                        backgroundColor: '#4BB0A5',
                    }
                ]
            }
        ],
      },
      {
        name: 'param',
        backgroundColor: '#4BB0A5',
        value: 1.1,
        children:[
            {
            name: 'param',
            backgroundColor: '#4BB0A5',
            value: 1.1,
            children:[
                {
                    name: 'param',
                    backgroundColor: '#4BB0A5',
                    value: 1.0,
                    children:[
                        {
                            name: 'param',
                            backgroundColor: '#4BB0A5',
                            value: 1.0,
                        }
                    ]
                },
                {
                    name: 'param',
                    backgroundColor: '#4BB0A5',
                    value: 0.1,
                    children:[
                        {
                            name: 'param',
                            backgroundColor: '#4BB0A5',
                            value: 0.1,
                        }
                    ]
                }
            ]
            }
    ]
      },
      {
        name: 'Swapper',
        backgroundColor: '#4BB0A5',
        value: 1.5,
        children:[
            {
                name: 'Swapper',
                backgroundColor: '#4BB0A5',
                value: 0.7,
                children:[
                    {
                        name: 'Swapper',
                        backgroundColor: '#4BB0A5',
                        value: 0.4,
                        children:[
                            {
                                name: 'Swapper',
                                backgroundColor: '#4BB0A5',
                                value: 0.4,
                            }
                        ]
                    },
                    {
                        name: 'Swapper',
                        backgroundColor: '#4BB0A5',
                        value: 0.2,
                        children:[
                            {
                                name: 'Swapper',
                                backgroundColor: '#4BB0A5',
                                value: 0.2,
                            }
                        ]
                    },
                    {
                        name: 'Swapper',
                        backgroundColor: '#4BB0A5',
                        value: 0.1,
                        children:[
                            {
                                name: 'Swapper',
                                backgroundColor: '#4BB0A5',
                                value: 0.1,
                            }
                        ]
                    }
                ]
            },
            {
                name: 'Swapper',
                backgroundColor: '#4BB0A5',
                value: 0.8,
                children:[
                    {
                        name: 'Swapper',
                        backgroundColor: '#4BB0A5',
                        value: 0.8,   
                        children:[
                            {
                                name: 'Swapper',
                                backgroundColor: '#4BB0A5',
                                value: 0.8,                            }
                        ]                 
                    }
                ]
            }
        ]
      },
      {
        name: 'wrk',
        backgroundColor: '#4BB0A5',
        value: 0.9,
        children:[
            {
                name: 'wrk',
                backgroundColor: '#4BB0A5',
                value: 0.6,
                children:[
                    {
                        name: 'wrk',
                        backgroundColor: '#4BB0A5',
                        value: 0.6,
                    }
                ]
            },
            {
                name: 'wrk',
                backgroundColor: '#4BB0A5',
                value: 0.2,
                children:[
                    {
                        name: 'wrk',
                        backgroundColor: '#4BB0A5',
                        value: 0.2,
                        children:[
                            {
                                name: 'wrk',
                                backgroundColor: '#4BB0A5',
                                value: 0.2,
                            }
                        ]
                    }
                ]
            },
            {
                name: 'wrk',
                backgroundColor: '#4BB0A5',
                value: 0.1,
                children:[
                    {
                        name: 'wrk',
                        backgroundColor: '#4BB0A5',
                        value: 0.1,
                        children:[
                            {
                                name: 'wrk',
                                backgroundColor: '#4BB0A5',
                                value: 0.1, 
                            }
                        ]
                    }
                ]
            }
        ]
      },
      {
        name: 'signoz',
        backgroundColor: '#4BB0A5',
        value: 0.1,
        children:[
            {
                name: 'signoz',
                backgroundColor: '#4BB0A5',
                value: 0.1,
                children:[
                    {
                        name: 'signoz',
                        backgroundColor: '#4BB0A5',
                        value: 0.1,
                        children:[
                            {
                                name: 'signoz',
                                backgroundColor: '#4BB0A5',
                                value: 0.1,
                            }
                        ]
                    }
                ]
            }
        ]
      },
    ]
  };

  export default data