import React from 'react';
import Linegraph from './TypesOfGraphs/linegraph'
import Bargraph from './TypesOfGraphs/bargraphs'
import PieChart from './TypesOfGraphs/piechart'
import Polararea from './TypesOfGraphs/polararea'
import data from '../Graph/flamechartData';
import { FlameGraph } from 'react-flame-graph';


const Graphs=(props)=>{
    const params=window.location.pathname
    // const params = useParams();
    console.log(params)
return( 
<div className='Graphs container'>
    <div className='row'>
        <div className='col-lg-6 col-sm-6'>
            {params==='/payment/application'?
            <FlameGraph
                data={data}
                height={200}
                width={400}
                onChange={node => {
                console.log(`"${node.name}" focused`);}}
            />
            :<Linegraph/>
            }
        </div>
        <div className='col-lg-6 col-sm-6'>
        {params==='/payment/application'?
            <FlameGraph
                data={data}
                height={200}
                width={400}
                onChange={node => {
                console.log(`"${node.name}" focused`);}}
            />
            :<Bargraph/>
            }        
        </div>
    </div>
    <div className='row'>
        <div className='col-lg-6 col-sm-6'>
            {params==='/payment/application'?
            <FlameGraph
                data={data}
                height={200}
                width={400}
                onChange={node => {
                console.log(`"${node.name}" focused`);}}
            />
            :<PieChart/>
            }
        </div>
        <div className='col-lg-6 col-sm-6'>
            {params==='/payment/application'?
            <FlameGraph
                data={data}
                height={200}
                width={400}
                onChange={node => {
                console.log(`"${node.name}" focused`);}}
            />
            :<Polararea/>
            }
        </div>
    </div>
</div>
)}

export default Graphs