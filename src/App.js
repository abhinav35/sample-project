import React,{Component} from 'react';
import './App.css';
import Dashboard from './Components/dashboard';
import {BrowserRouter as Router,Route} from 'react-router-dom';
class App extends Component{
  render() {
    return (
      <div>
        <Router>
          <Route path='/' component={Dashboard}/>
        </Router>
      </div>
    )
  }
}
export default App